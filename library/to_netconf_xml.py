#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pprint
from typing import Dict, Any, Optional
from ansible.module_utils.basic import AnsibleModule
from lxml.etree import Element, SubElement, QName, tostring


PP = pprint.PrettyPrinter(indent=4)


class EricssonXMLNamespaces():
    ctxsr6k = "urn:rdns:com:ericsson:oammodel:ericsson-contexts-router6000"
    snmpr6k = "urn:rdns:com:ericsson:oammodel:ericsson-snmp-router6000"


MAPPING_CONTEXTES = {
    'ctxsr6k': EricssonXMLNamespaces.ctxsr6k,
    'snmpr6k': EricssonXMLNamespaces.snmpr6k
}


class NetconfXMLConverter():
    full_data = Dict[str, Any]

    parent_xml = Optional[Dict[str, Any]]
    current_xml = Optional[Dict[str, Any]]
    yang_data = Optional[Dict[str, Any]]
    namespace = Optional[str]
    prefix = Optional[str]
    nsmap = Optional[Dict[str, str]]
    root_parent = Optional[bytes]
    root_protocol = Optional[bytes]

    def __init__(self, full_data):
        assert 'xml_config' in full_data.keys()
        assert 'current' in full_data.get('xml_config').keys()

        self.full_data = full_data
        self.parent_xml = self.full_data.get('xml_config') \
                                        .get('parent', None)
        self.current_xml = self.full_data.get('xml_config') \
                                         .get('current', None)

        self.yang_data = self.full_data.get(
            self.full_data.get('xml_config').get('current').get('key'),
            None
        )

        self.namespace = self.current_xml.get('namespace', None)
        self.prefix = self.current_xml.get('prefix', None)
        self.get_nsmap()
        self.create_root()

        self.to_xml(self.root_protocol, self.yang_data)

    def get_root(self) -> bytes:
        return self.root_parent

    def get_root_str(self) -> str:
        return str(tostring(self.root_parent)).replace('\\', '')

    def get_nsmap(self) -> Dict[str, str]:
        self.nsmap = dict()
        if self.parent_xml is not None:
            for i in self.parent_xml.get('namespaces'):
                self.nsmap[i] = MAPPING_CONTEXTES.get(i)
        elif self.current_xml is not None:
            for i in self.current_xml.get('namespaces'):
                self.nsmap[i] = MAPPING_CONTEXTES.get(i)

    def create_root(self) -> bytes:
        if self.parent_xml is not None:
            self.root_parent = Element(
                QName(
                    MAPPING_CONTEXTES.get(self.parent_xml.get('ns_prefix')),
                    self.parent_xml.get('key')
                ),
                nsmap=self.nsmap
            )

            self.root_protocol = SubElement(
                self.root_parent,
                QName(
                    MAPPING_CONTEXTES.get(self.current_xml.get('ns_prefix')),
                    self.current_xml.get('key')
                ),
                nsmap=self.nsmap
            )

        elif self.current_xml is not None:
            self.root_parent = self.root_protocol = Element(
                QName(
                    MAPPING_CONTEXTES.get(self.current_xml.get('ns_prefix')),
                    self.current_xml.get('key')
                ),
                nsmap=self.nsmap
            )

    def to_xml(self, root_point, data):
        for key, value in data.items():
            if value == {}:
                self.to_xml_add_subelement(root_point, key)

            elif isinstance(value, dict):
                new_root = self.to_xml_add_subelement(root_point, key)
                self.to_xml(new_root, value)

            elif isinstance(value, list):
                for line in value:
                    new_root = self.to_xml_add_subelement(root_point, key)
                    self.to_xml(new_root, line)

            else:
                self.to_xml_add_leaf(root_point, key, value)

    def to_xml_add_leaf(self, root_point, key, value):
        if isinstance(value, list):
            for i in value:
                t = SubElement(
                    root_point,
                    QName(
                        MAPPING_CONTEXTES.get(
                            self.current_xml.get('ns_prefix')),
                        key
                    ),
                    nsmap=self.nsmap
                ).text = str(i)
        else:
            t = SubElement(
                root_point,
                QName(
                    MAPPING_CONTEXTES.get(self.current_xml.get('ns_prefix')),
                    key
                ),
                nsmap=self.nsmap
            ).text = str(value)

        return t

    def to_xml_add_subelement(self, root_point, key) -> None:
        return SubElement(
            root_point,
            QName(
                MAPPING_CONTEXTES.get(self.current_xml.get('ns_prefix')),
                key
            ),
            nsmap=self.nsmap
        )

    def print(self) -> None:
        PP.pprint(self.dict())

    def print_root(self) -> None:
        print(tostring(self.root_parent))

    def dict(self) -> Dict[str, Any]:
        return {
            "<full_data>": self.full_data,
            "<parent_xml>": self.parent_xml,
            "<current_xml>": self.current_xml,
            "<yang_data>": self.yang_data,
            "<namespace>": self.namespace,
            "<prefix>": self.prefix,
            "<nsmap>": self.nsmap,
            "<root_parent>": self.root_parent,
            "<root_protocol>": self.root_protocol
        }


def main():
    module = AnsibleModule(
        argument_spec={
            'content': {
                'required': True, "type": "dict"
            }
        }
    )

    xml = NetconfXMLConverter(module.params.get('content'))
    module.exit_json(changed=True, result=xml.get_root_str())


if __name__ == "__main__":
    main()
