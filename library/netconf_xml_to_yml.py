#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import xmltodict
from typing import Dict, Any, Optional
from ansible.module_utils.basic import AnsibleModule
from lxml.etree import Element, SubElement, QName, tostring


class NetconfXMLToYAML(object):
    xml_payload: str
    dict_transit: Optional[Dict[Any, Any]]
    dict_result: Optional[Dict[Any, Any]]

    EXCLUDE_KEYS = ['rpc-reply']
    EXCLUDE_CHAR_CONTAINS = ['@']

    def __init__(
        self,
        xml_payload: str='',
        dict_transit: Optional[Dict[Any, Any]] = {},
        dict_result: Optional[Dict[Any, Any]] = {}
    ):
        self.xml_payload = xml_payload
        self.dict_transit = self._format_xml_to_dict(self.xml_payload)
        self.dict_result = self._format_xml(self.xml_payload)

    def _format_xml(self, xml_payload):
        temp = dict()
        for key, value in self.dict_transit.get('rpc-reply').get('data').items():
            temp[key] = value
        return temp

    def _format_xml_to_dict(self, xml_payload) -> Dict[str, Any]:
        return dict(xmltodict.parse(xml_payload))

    def _remove(self, key) -> bool:
        return self._exclude_char_in_key(key) and key in self.EXCLUDE_KEYS

    def _exclude_char_in_key(self, key) -> bool:
        for c in self.EXCLUDE_CHAR_CONTAINS:
            if c in key:
                return True
        return False


def main():
    module = AnsibleModule(
        argument_spec={
            'xml_payload': {
                'required': True, "type": "str"
            }
        }
    )

    obj = NetconfXMLToYAML(module.params.get('xml_payload'))

    module.exit_json(changed=True, result=obj.dict_result)


if __name__ == "__main__":
    main()
