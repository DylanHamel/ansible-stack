#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from abc import ABC
from typing import Optional, Dict, Any
from ansible.module_utils.basic import AnsibleModule


class BaseGenerator(ABC):
    ansible_module: AnsibleModule
    host_vars: Dict[str, Any]
    formatted_data: Optional[Dict[str, Any]]

    def __init__(
        self,
        ansible_module: AnsibleModule = None,
        host_vars: Dict[str, Any] = {},
        formatted_data: Optional[Dict[str, Any]] = {}
    ):
        if ansible_module is None:
            self.return_error(
                f"{type(self)} - ansible_module can't be None"
            )

        if host_vars is None:
            self.return_error(
                f"{type(self)} - host_vars can't be None"
            )
        elif not isinstance(host_vars, dict):
            self.host_vars = self.convert_to_dict(host_vars)
        else:
            self.host_vars = host_vars

        self.formatted_data = formatted_data

    def convert_to_dict(self, data) -> Dict[Any, Any]:
        if not isinstance(data, dict):
            try:
                return json.loads(data)
            except Exception:
                self.return_error(
                    f"{type(self)} - can't convert to dict"
                )

    def dict(self) -> Dict[str, Any]:
        return {
            'host_vars': self.host_vars,
            'formatted_data': self.formatted_data
        }

    def dict_full(self) -> Dict[str, Any]:
        return {
            **{
                'formatted_data_keys()': list(self.formatted_data.keys())
            },
            **self.dict()
        }

    def print_txt(self) -> None:
        print(self.dict())

    def print(self) -> None:
        print(json.dumps(self.dict(), indent=4, sort_keys=True))

    def print_full(self) -> None:
        print(json.dumps(self.dict_full(), indent=4, sort_keys=True))

    def return_error(self, msg) -> None:
        self.ansible_module.fail_json(msg=msg)


class EricssonGenerator(BaseGenerator):

    def __init__(
        self,
        ansible_module: AnsibleModule = None,
        host_vars: Dict[str, Any] = {},
        formatted_data: Optional[Dict[str, Any]] = {}
    ):
        super().__init__(
            ansible_module=ansible_module,
            host_vars=host_vars,
            formatted_data=formatted_data
        )
        self.__format_context()
        self.__format_data()

    def __format_context(self) -> None:
        self.formatted_data['contexts_vars'] = dict()
        self.formatted_data['contexts_vars']['contexts'] = dict()
        if 's_contexts' in self.host_vars.keys():
            for context in self.host_vars.get('s_contexts'):
                self.formatted_data.get('contexts_vars') \
                                   .get(
                                       'contexts'
                                    )[context.get('name')] = dict()
                for key, values in self.host_vars.items():
                    if f"y_{context.get('alias')}" in key:
                        for subkey, subvalues in values.items():
                            self.formatted_data.get('contexts_vars') \
                                               .get('contexts') \
                                               .get(
                                                   context.get('name')
                                                )[subkey] = subvalues

    def __format_data(self) -> None:
        for key, values in self.host_vars.items():
            if "y_global" in key:
                if isinstance(values, dict):
                    for k, v in values.items():
                        self.formatted_data[k] = v

            elif "y_context" in key:
                if isinstance(values, dict):
                    for k, v in values.items():
                        self.formatted_data['contexts_vars'][k] = v


def main():
    module = AnsibleModule(
        argument_spec={
            "my_param": dict(required=True),
            "host_data": dict(required=True)
        }
    )

    eg = EricssonGenerator(module, module.params.get('host_data'))
    module.exit_json(changed=False, result=eg.formatted_data, test=123)


if __name__ == "__main__":
    main()
