Feature: Test YML to XML converter.
    Scenario: Open a XML file from Ericsson devices and compare to a YAML file.
    Given I open a XML file and store content in a lxml object named o0001
    And I open a YAML file and convert content to lxml object nanmed o0002
    Then Object o0001 from XML file is equal to object o0002
