#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
from library.to_netconf_xml import NetconfXMLConverter
from lxml.etree import Element, SubElement, QName, tostring, parse, XMLParser, XML


PATH_FEATURE = './tests/features/'
PATH_TO_OUTPUTS = f'{PATH_FEATURE}/outputs'
PATH_TO_CONFIG = f'{PATH_FEATURE}/configs'


@given(u'I open a XML file and store content in a lxml object named o0001')
def step_impl(context):
    with open(f'{PATH_TO_OUTPUTS}/snmp_001.xml', 'r') as f:
        xml_content = f.read()

    parser = XMLParser(remove_blank_text=True)
    context.o0001 = XML(xml_content, parser=parser)


@given(u'I open a YAML file and convert content to lxml object nanmed o0002')
def step_impl(context):
    with open(f'{PATH_TO_CONFIG}/y_snmp_001.yml', 'r') as f:
        data = yaml.load(f.read(), Loader=yaml.FullLoader)
    context.o0002 = NetconfXMLConverter(data.get('y_snmp'))


@then(u'Object o0001 from XML file is equal to object o0002')
def step_impl(context):
    assert tostring(context.o0002.root_parent) == tostring(context.o0001)
