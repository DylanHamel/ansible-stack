module ericsson-xc-group-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-xc-group-ipos";

  prefix "xcgroupipos";

  import ietf-interfaces {
    prefix "if";
  }

  import ericsson-ethernet-ext-ipos {
    prefix "ethxipos";
  }

  import ericsson-lag-ext-ipos {
    prefix "lagxipos";
  }

  import ericsson-vpws-ipos {
    prefix "vpwsipos";
  }

  import ericsson-l2-service-ipos {
    prefix "l2serviceipos";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-xc-group-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-06-13" {
    description
      "initial revision";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  grouping config-xc-group-grp {
    description
      "XC group configuration mode";
    /*
    xc lg (lg-name) <service-instance (si-id-in)> [(end-si-id-
     in)] to { <slot-port-out (slot-port-id-out)<service-
     instance-out (si-id-out)> [(end-si-id-out)]> | <lg (lg-name)
     <service-instance-out (si-id-out)> [(end-si-id-out)]> |
     <pseudowire instance (pw-inst-id)> }
    */
    list xc-lg {
      key "lg-name service-instance";
      description
        "Link-group of circuit(s)";
      leaf lg-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        description
          "Linkgroup name";
      }
      leaf service-instance {
        type leafref {
          path "/if:interfaces/if:interface/lagxipos:link-"
          + "group/l2serviceipos:service-instance/l2serviceipos"
          + ":spec-instance-id";
        }
        description
          "Service instance identifier.";
      }
      leaf end-si-id-in {
        type uint32;
        must "number(/if:interfaces/if:interface/lagxipos:link-"
        + "group/l2serviceipos:service-instance/l2serviceipos:end-"
        + "instance-id) >= number(.)" {
          error-message "invalid length or prefix range.";
          description
            "";
        }
        description
          "End identifier of the consecutive range of service
           instances.";
      }
      leaf to {
        type empty;
        mandatory true;
        description
          "Configure the outbound circuit to be crossconnected
           to";
      }
      choice xc-lg-choice {
        mandatory true;
        description
          "Xc lg circuit choice";
        case slot-port-out {
          container slot-port-out {
            description
              "outbound circuit";
            leaf slot-port-id-out {
              type leafref {
                path "/if:interfaces/if:interface/if:name";
              }
              mandatory true;
              description
                "Specify outbound circuit";
            }
            leaf service-instance-out {
              type leafref {
                path "/if:interfaces/if:interface/ethxipos:ether"
                + "net/l2serviceipos:service-instance/l2serviceipos"
                + ":spec-instance-id";
              }
              mandatory true;
              description
                "Service instance identifier.";
            }
            leaf end-si-id-out {
              type uint32;
              must "number(/if:interfaces/if:interface/ethxipos:"
              + "ethernet/l2serviceipos:service-"
              + "instance/l2serviceipos:end-instance-id) >= "
              + "number(.)" {
                error-message "invalid length or prefix range.";
                description
                  "";
              }
              description
                "End identifier of the consecutive range of
                 service instances.";
            }
          }
        }
        case lg {
          container lg {
            description
              "Link-group of circuit(s)";
            leaf lg-name {
              type leafref {
                path "/if:interfaces/if:interface/if:name";
              }
              mandatory true;
              description
                "Linkgroup name";
            }
            leaf service-instance-out {
              type leafref {
                path "/if:interfaces/if:interface/lagxipos:link-"
                + "group/l2serviceipos:service-"
                + "instance/l2serviceipos:spec-instance-id";
              }
              mandatory true;
              description
                "Service instance identifier.";
            }
            leaf end-si-id-out {
              type uint32;
              must "number(/if:interfaces/if:interface/lagxipos"
              + ":link-group/l2serviceipos:service-"
              + "instance/l2serviceipos:end-instance-id) >= "
              + "number(.)" {
                error-message "invalid length or prefix range.";
                description
                  "";
              }
              description
                "End identifier of the consecutive range of
                 service instances.";
            }
          }
        }
        case pseudowire {
          container pseudowire {
            description
              "VLL cross connection";
            leaf instance {
              type leafref {
                path "/ctxsipos:contexts/vpwsipos:pseudowire/vpw"
                + "sipos:instance/vpwsipos:instance";
              }
              mandatory true;
              description
                "Configure a Pseudowire Instance";
            }
          }
        }
      }
    }
    /*
    xc (slot-port-id) <service-instance (si-id-in)> [(end-si-id-
     in)] to { <slot-port-out (slot-port-id-out)<service-
     instance-out (si-id-out)> [(end-si-id-out)]> | <lg (lg-name)
     <service-instance-out (si-id-out)> [(end-si-id-out)]> |
     <pseudowire instance (pw-inst-id)> }
    */
    list xc {
      key "slot-port-id service-instance";
      description
        "Configure a circuit crossconnect";
      leaf slot-port-id {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        description
          "input circuit(s)";
      }
      leaf service-instance {
        type leafref {
          path "/if:interfaces/if:interface/ethxipos:ethernet/l2"
          + "serviceipos:service-instance/l2serviceipos:spec-"
          + "instance-id";
        }
        description
          "Service instance identifier.";
      }
      leaf end-si-id-in {
        type uint32;
        must "number(/if:interfaces/if:interface/ethxipos:ethern"
        + "et/l2serviceipos:service-instance/l2serviceipos:end-"
        + "instance-id) >= number(.)" {
          error-message "invalid length or prefix range.";
          description
            "";
        }
        description
          "End identifier of the consecutive range of service
           instances.";
      }
      leaf to {
        type empty;
        mandatory true;
        description
          "Configure the outbound circuit to be crossconnected
           to";
      }
      choice xc-choice {
        mandatory true;
        description
          "Xc slot-port circuit choice";
        case slot-port-out {
          container slot-port-out {
            description
              "outbound circuit";
            leaf slot-port-id-out {
              type leafref {
                path "/if:interfaces/if:interface/if:name";
              }
              mandatory true;
              description
                "Specify outbound circuit";
            }
            leaf service-instance-out {
              type leafref {
                path "/if:interfaces/if:interface/ethxipos:ether"
                + "net/l2serviceipos:service-instance/l2serviceipos"
                + ":spec-instance-id";
              }
              mandatory true;
              description
                "Service instance identifier.";
            }
            leaf end-si-id-out {
              type uint32;
              must "number(/if:interfaces/if:interface/ethxipos:"
              + "ethernet/l2serviceipos:service-"
              + "instance/l2serviceipos:end-instance-id) >= "
              + "number(.)" {
                error-message "invalid length or prefix range.";
                description
                  "";
              }
              description
                "End identifier of the consecutive range of
                 service instances.";
            }
          }
        }
        case lg {
          container lg {
            description
              "Link-group of circuit(s)";
            leaf lg-name {
              type leafref {
                path "/if:interfaces/if:interface/if:name";
              }
              mandatory true;
              description
                "Linkgroup name";
            }
            leaf service-instance-out {
              type leafref {
                path "/if:interfaces/if:interface/lagxipos:link-"
                + "group/l2serviceipos:service-"
                + "instance/l2serviceipos:spec-instance-id";
              }
              mandatory true;
              description
                "Service instance identifier.";
            }
            leaf end-si-id-out {
              type uint32;
              must "number(/if:interfaces/if:interface/lagxipos"
              + ":link-group/l2serviceipos:service-"
              + "instance/l2serviceipos:end-instance-id) >= "
              + "number(.)" {
                error-message "invalid length or prefix range.";
                description
                  "";
              }
              description
                "End identifier of the consecutive range of
                 service instances.";
            }
          }
        }
        case pseudowire {
          container pseudowire {
            description
              "VLL cross connection";
            leaf instance {
              type leafref {
                path "/ctxsipos:contexts/vpwsipos:pseudowire/vpw"
                + "sipos:instance/vpwsipos:instance";
              }
              mandatory true;
              description
                "VLL cross connection";
            }
          }
        }
      }
    }
  }

  augment "/ctxsipos:contexts" {
    description
      "ericsson-xc-group";
    /*
    xc-group {(group-name) | default }
    */
    list xc-group {
      key "xc-group";
      description
        "Configure crossconnect group";
      leaf xc-group {
        type union {
          type string;
          type enumeration {
            enum default {
              value 0;
              description
                "Default group name";
            }
          }
        }
        description
          "Xc groupname or default";
      }
      uses config-xc-group-grp;
    }
  }

}
