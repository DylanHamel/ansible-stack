module ericsson-bfd-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-bfd-ipos";

  prefix "bfdipos";

  import ietf-inet-types {
    prefix "inet";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-bfd-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-06-11" {
    description
      "IPOS-11510 Libnetconf BFD Please print the correct error
       message, and add the when/must in the bfd model";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-05-21" {
    description
      "IPOS-10745 New command for model BFD";
    reference
      "rfc6020";
  }

  revision "2017-04-21" {
    description
      "IPOS-9165";
    reference
      "rfc6020";
  }

  revision "2017-04-13" {
    description
      "IPOS-7421 [BFD] neighbor data is inconsistent between
       exec cli and confd when deleting multihop";
    reference
      "rfc6020";
  }

  revision "2017-01-18" {
    description
      "recover multihop and TTL";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature passive {
    description
      "This feature indicates that the device supports passive
       mode in BFD.";
  }

  grouping bfd-grp {
    description
      "BFD configuration mode";
    /*
    neighbor { (nbr-addr) } [ multihop interface (intf) ]
    */
    list neighbor {
      key "nbr-addr";
      description
        "BFD Neighbor";
      leaf nbr-addr {
        type inet:ip-address;
        description
          "IP address of neighbor";
      }
      container multihop {
        description
          "Multihop session";
        leaf interface {
          type leafref {
            path "/if:interfaces/if:interface/if:name";
          }
          description
            "source Interface";
        }
      }
      uses bfd-nbr-grp;
    }
    /*
    interface (intf-name)
    */
    list interface {
      key "interface";
      description
        "BFD interface";
      leaf interface {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        description
          "Interface to enable BFD on";
      }
      uses bfd-if-grp;
    }
    /*
    micro-neighbor { (nbr-addr) | (addr-v6) } (lag-name) {
     (local-addr) | (addr-v6) }
    */
    list micro-neighbor {
      when "../../../ctxipos:context-name='local'" {
        description
          "";
      }
      key "lag-name";
      description
        "Micro-BFD Neighbor";
      leaf lag-name {
        type string;
        description
          "Lag name";
      }
      leaf micro-neighbor-choice {
        type union {
          type inet:ip-address;
          type inet:ipv6-address;
        }
        mandatory true;
        description
          "leaf micro-neighbor-choice.";
      }
      leaf micro-neighbor-choice1 {
        type union {
          type inet:ip-address;
          type inet:ipv6-address;
        }
        mandatory true;
        description
          "leaf micro-neighbor-choice1.";
      }
      uses micro-bfd-nbr-grp;
    }
    /*
    snmp trap all
    */
    leaf snmp-trap-all {
      type empty;
      description
        "Enable sending BFD session traps";
    }
    /*
    log-neighbor-changes
    */
    leaf log-neighbor-changes {
      type empty;
      description
        "Enable BFD Syslog";
    }
  }

  grouping bfd-if-grp {
    description
      "BFD interface configuration mode";
    /*
    minimum { transmit-interval { 3.3 | 10 | 20 | 50 | 100 |
     1000 } | receive-interval { 3.3 | 10 | 20 | 50 | 100 | 1000
     } }
    */
    container minimum {
      description
        "BFD minimum intervals";
      leaf transmit-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum desired transmit interval";
      }
      leaf receive-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum required receive interval";
      }
    }
    /*
    detection-multiplier (mult-value)
    */
    leaf detection-multiplier {
      type uint8 {
        range "1..255";
      }
      default "3";
      description
        "Detect time multiplier";
    }
  }

  grouping bfd-nbr-grp {
    description
      "BFD neighbor configuration mode";
    /*
    minimum { transmit-interval { 3.3 | 10 | 20 | 50 | 100 |
     1000 } | receive-interval { 3.3 | 10 | 20 | 50 | 100 | 1000
     } }
    */
    container minimum {
      description
        "BFD minimum intervals";
      leaf transmit-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum desired transmit interval";
      }
      leaf receive-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum required receive interval";
      }
    }
    /*
    detection-multiplier (mult-value)
    */
    leaf detection-multiplier {
      type uint8 {
        range "1..255";
      }
      default "3";
      description
        "Detect time multiplier";
    }
    /*
    passive
    */
    leaf passive {
      when "../bfdipos:multihop/bfdipos:interface" {
        description
          "";
      }
      if-feature passive;
      type empty;
      description
        "Accept session initiation by neighbor";
    }
    /*
    ttl { (ttl-value) | minimum-accepted (ttl-value) }
    */
    container ttl {
      when "../bfdipos:multihop/bfdipos:interface" {
        description
          "";
      }
      description
        "Time-to-live";
      leaf ttl-value {
        type uint8 {
          range "1..255";
        }
        default "255";
        description
          "Specify ttl for transmitted BFD packets";
      }
      leaf minimum-accepted {
        type uint8 {
          range "1..255";
        }
        default "1";
        description
          "Minimum ttl accepted in received BFD packets";
      }
    }
  }

  grouping micro-bfd-nbr-grp {
    description
      "micro BFD neighbor configuration mode";
    /*
    minimum { transmit-interval { 3.3 | 10 | 20 | 50 | 100 |
     1000 } | receive-interval { 3.3 | 10 | 20 | 50 | 100 | 1000
     } }
    */
    container minimum {
      description
        "BFD minimum intervals";
      leaf transmit-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum desired transmit interval";
      }
      leaf receive-interval {
        type enumeration {
          enum 3.3 {
            value 0;
            description
              "3.3 ms interval";
          }
          enum 10 {
            value 1;
            description
              "10 ms interval";
          }
          enum 20 {
            value 2;
            description
              "20 ms interval";
          }
          enum 50 {
            value 3;
            description
              "50 ms interval";
          }
          enum 100 {
            value 4;
            description
              "100 ms interval";
          }
          enum 1000 {
            value 5;
            description
              "1000 ms interval";
          }
        }
        default "1000";
        description
          "Minimum required receive interval";
      }
    }
    /*
    detection-multiplier (mult-value)
    */
    leaf detection-multiplier {
      type uint8 {
        range "1..255";
      }
      default "3";
      description
        "Detect time multiplier";
    }
  }

  augment "/ctxsipos:contexts/ctxipos:context/ctxipos:router" {
    description
      "ericsson-bfd";
    /*
    bfd
    */
    container bfd {
      presence "";
      description
        "Bidirectional Forwarding Detection (BFD)";
      uses bfd-grp;
    }
  }

}
