module ericsson-lag-ext-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-lag-ext-ipos";

  prefix "lagxipos";

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import iana-if-type {
    prefix "ianaift";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ietf-yang-types {
    prefix "yang";
  }

  organization
    "Ericsson";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-lag-ext-ipos
     Copyright (c) 2015-2016 Ericsson AB.
     All rights reserved";

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature economical {
    description
      "This feature indicates that the device supports
       economical in lag.";
  }

  feature link-pinning {
    description
      "This feature indicates that the device supports link-
       pinning in lag.";
  }

  feature load-balance {
    description
      "This feature indicates that the device supports load-
       balance in lag.";
  }

  grouping lag-grp {
    description
      "Unified Link group configuration mode";
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind link-group Bind an interface to the link-group";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Interface name";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name";
      }
    }
    /*
    mac-address { auto | port | mac-addr }
    */
    container mac-address {
      description
        "Configure MAC address";
      choice mac-address {
        description
          "IPOS choice";
        case auto {
          leaf auto {
            type empty;
            description
              "Specify software generated link-group Source MAC
               address";
          }
        }
        case port {
          leaf port {
            type empty;
            description
              "Specify MAC address is to be inherited from a
               constituent port";
          }
        }
        case mac-addr {
          leaf mac-addr {
            type yang:mac-address;
            description
              "Source MAC Address";
          }
        }
      }
    }
    /*
    maximum-links (lag-num)
    */
    leaf maximum-links {
      type uint8 {
        range "1..8";
      }
      description
        "Specify the maximum number of active links in the group
         Maximum number of links allowed for the group to be
         aggregated";
    }
    /*
    load-balance
    */
    leaf load-balance {
      if-feature load-balance;
      type boolean;
      default "true";
      description
        "Enable/Disable load-balance";
    }
    /*
    minimum-links (uplinks-num)
    */
    leaf minimum-links {
      type uint8 {
        range "1..8";
      }
      description
        "Minimum number of links up required for the group to be
         up Minimum number of functional links required for the
         group to be up";
    }
    /*
    lacp [ admin-key (key-val) | damp-timeout (timeout-val) |
     periodic-timeout { short [immediate] | long } | revertible |
     passive | active | ignore-system-id | system-id priority
     (priority-val) [mac-address (mac-addr)]]
    */
    container lacp {
      presence "";
      description
        "Configure LACP parameters";
      leaf admin-key {
        type uint16 {
          range "32767..65535";
        }
        description
          "Configure LACP Administrative Key Specify the Admin-
           key value";
      }
      leaf damp-timeout {
        type uint8 {
          range "0..10";
        }
        description
          "Timer to damp LAG-state flapping from constituent-
           state changes Specify the damp timeout in seconds";
      }
      container periodic-timeout {
        description
          "Configure LACP Periodic Timeout";
        choice periodic-timeout {
          description
            "IPOS choice";
          case short {
            container short {
              presence "";
              description
                "Configure LACP Short Periodic Timeout";
              leaf immediate {
                type empty;
                description
                  "Skip the Expired State upon LACP Timeout";
              }
            }
          }
          case long {
            leaf long {
              type empty;
              description
                "Configure LACP Long Periodic Timeout";
            }
          }
        }
      }
      leaf revertible {
        type boolean;
        default "true";
        description
          "Enable/Disable revertible";
      }
      leaf passive {
        type empty;
        description
          "Configure LACP in Passive Mode";
      }
      leaf active {
        type empty;
        description
          "Configure LACP in Active Mode";
      }
      leaf ignore-system-id {
        type empty;
        description
          "Configure LACP Ignore System Id";
      }
      container system-id-priority {
        presence "";
        description
          "Configure system parameters for link-group Configure
           LACP system priority";
        leaf priority-val {
          type uint16 {
            range "0..65534";
          }
          mandatory true;
          description
            "LACP system priority value";
        }
        leaf mac-address {
          type yang:mac-address;
          description
            "Configure LACP system MAC MAC Address";
        }
      }
    }
    /*
    encapsulation dot1q
    */
    leaf encapsulation-dot1q {
      type empty;
      description
        "Set encapsulation encapsulate dot1q";
    }
  }

  augment "/if:interfaces/if:interface" {
    description
      "ericsson-lag";
    /*
    link-group [ pos | economical | link-pinning ]
    */
    container link-group {
      when "../if:type = 'ianaift:ieee8023adLag'" {
        description
          "";
      }
      presence "";
      description
        "Define a link group";
      choice link-group-opt {
        description
          "IPOS choice";
        case pos {
          leaf pos {
            type empty;
            description
              "Define a POS type group";
          }
        }
        case economical {
          leaf economical {
            if-feature economical;
            type empty;
            description
              "Set circuit defaults to non-replicated and
               circuit homing";
          }
        }
        case link-pinning {
          leaf link-pinning {
            if-feature link-pinning;
            type empty;
            description
              "Set circuit defaults to replicated and circuit
               homing";
          }
        }
      }
      uses lag-grp;
    }
  }

}
