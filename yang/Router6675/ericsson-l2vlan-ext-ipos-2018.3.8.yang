module ericsson-l2vlan-ext-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-l2vlan-ext-ipos";

  prefix "l2vlanxipos";

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import iana-if-type {
    prefix "ianaift";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ietf-yang-types {
    prefix "yang";
  }

  import ericsson-ethernet-ext-ipos {
    prefix "ethxipos";
  }

  import ericsson-lag-ext-ipos {
    prefix "lagxipos";
  }

  import ericsson-mirror-policy-ipos {
    prefix "mirrorpolicyipos";
  }

  import ericsson-bvi-ext-ipos {
    prefix "bvixipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-l2vlan-ext-ipos
     Copyright (c) 2018 Ericsson AB.
     All rights reserved";

  revision "2018-03-08" {
    description
      "remove the change for circuit-group-member under pvc";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "5";
  }

  revision "2018-01-23" {
    description
      "refine the error-message for encapsulation-dot1q must
       condition";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "4";
  }

  revision "2018-01-12" {
    description
      "Add circuit-group-member under pvc";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "3";
  }

  revision "2017-09-29" {
    description
      "Confd can support bvi QinQ configuration";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "2";
  }

  revision "2017-07-24" {
    description
      "Add new container 'mirror' in cfg-dot1q-pvc-grp";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "1";
  }

  revision "2017-07-15" {
    description
      "IPOS-11938 New command for subscribe micro-bfd";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-07-11" {
    description
      "Add new container 'mirror' in dot1q-pvc-grp";
    reference
      "rfc6020";
  }

  revision "2017-06-02" {
    description
      "New commands for Multicast";
    reference
      "rfc6020";
  }

  revision "2017-04-12" {
    description
      "Add when condition for dot1q.";
    reference
      "rfc6020";
  }

  revision "2017-01-11" {
    description
      "Add must condition for dot1q.";
    reference
      "rfc6020";
  }

  revision "2016-12-30" {
    description
      "For Side Effect difference modification.";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature link-pinning {
    description
      "This feature indicates that the device supports
       link-pinning in l2vlan.";
  }

  feature mac-address {
    description
      "This feature indicates that the device supports
       mac-address in l2vlan.";
  }

  feature qos-intf-ssr {
    description
      "This feature indicates that the device supports flow
       apply in l2vlan.";
  }

  grouping dot1q-pvc-grp {
    description
      "DOT1Q PVC configuration mode";
    /*
    circuit-group-member (grp-name)
    */
    leaf circuit-group-member {
      type string;
      description
        "Specify the circuit-group for this pvc(s)";
    }
    /*
    ipv4 multicast maximum-bandwidth (bw-val) { percent | Kbps |
     Mbps | Gbps }
    */
    container ipv4-multicast-maximum-bandwidth {
      presence "";
      description
        "Set max-bandwidth for subscribers";
      leaf bw-val {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "max-bandwidth in Kbps/Mbps/Gbps/percent";
      }
      choice ipv4-multicast-maximum-bandwidth-choice {
        default Mbps;
        description
          "IPOS choice";
        case percent {
          leaf percent {
            type empty;
            description
              "Input in percentage form";
          }
        }
        case Kbps {
          leaf Kbps {
            type empty;
            description
              "Kilo bits per sec";
          }
        }
        case Mbps {
          leaf Mbps {
            type empty;
            description
              "Mega bits per sec";
          }
        }
        case Gbps {
          leaf Gbps {
            type empty;
            description
              "Giga bits per sec";
          }
        }
      }
    }
    /*
    ipv6 multicast maximum-bandwidth (bw-val) { percent | Kbps |
     Mbps | Gbps }
    */
    container ipv6-multicast-maximum-bandwidth {
      presence "";
      description
        "Set max-bandwidth for subscribers";
      leaf bw-val {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "max-bandwidth in Kbps/Mbps/Gbps/percent";
      }
      choice ipv6-multicast-maximum-bandwidth-choice {
        default Mbps;
        description
          "IPOS choice";
        case percent {
          leaf percent {
            type empty;
            description
              "Input in percentage form";
          }
        }
        case Kbps {
          leaf Kbps {
            type empty;
            description
              "Kilo bits per sec";
          }
        }
        case Mbps {
          leaf Mbps {
            type empty;
            description
              "Mega bits per sec";
          }
        }
        case Gbps {
          leaf Gbps {
            type empty;
            description
              "Giga bits per sec";
          }
        }
      }
    }
    /*
    description (desc-str)
    */
    leaf description {
      type string;
      description
        "set description string";
    }
    /*
    enabled
    */
    leaf enabled {
      type boolean;
      default "true";
      description
        "Shutdown the PVC";
    }
    /*
    mac-address (mac-addr)
    */
    leaf mac-address {
      if-feature mac-address;
      type yang:mac-address;
      description
        "Configure MAC address";
    }
    /*
    subscribe micro-bfd
    */
    leaf subscribe-micro-bfd {
      type empty;
      description
        "Micro-bfd events for the link group";
    }
    /*
    mirror { <policy (mirror-name) { in | out } [source-tag {
     dot1q (tag-id) | dot1ad (tag-id) } ] [ <ip [ ipv6 ] acl-
     counters> | ipv6 acl-counters ]> | <destination (dest-name)>
     }
    */
    container mirror {
      description
        "Configure mirror policy or destination";
      choice mirror {
        description
          "IPOS choice";
        case policy {
          list policy {
            key "policy-choice";
            max-elements 2;
            description
              "Configure mirror policy";
            leaf policy-choice {
              type enumeration {
                enum in {
                  value 0;
                  description
                    "Configure inbound mirror policy";
                }
                enum out {
                  value 1;
                  description
                    "Configure outbound mirror policy";
                }
              }
              description
                "leaf policy-choice.";
            }
            leaf mirror-name {
              type leafref {
                path "/ctxsipos:contexts/mirrorpolicyipos"
                + ":mirror-policy/mirrorpolicyipos:mirror-polname";
              }
              mandatory true;
              description
                "Mirror policy name";
            }
            container source-tag {
              description
                "Configure mirror source tag";
              choice source-tag {
                description
                  "IPOS choice";
                case dot1q {
                  leaf dot1q {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1q Ethernet type";
                  }
                }
                case dot1ad {
                  leaf dot1ad {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1ad Ethernet type";
                  }
                }
              }
            }
            choice policy-opt1 {
              description
                "IPOS choice";
              case ip {
                container ip {
                  presence "";
                  description
                    "Configure IP attributes";
                  leaf ipv6 {
                    type empty;
                    description
                      "Configure IPV6 attributes";
                  }
                  leaf acl-counters {
                    type empty;
                    mandatory true;
                    description
                      "Enable ACL counters";
                  }
                }
              }
              case ipv6-acl-counters {
                leaf ipv6-acl-counters {
                  type empty;
                  description
                    "Enable ACL counters";
                }
              }
            }
          }
        }
        case destination {
          leaf destination {
            type string {
              length "1..39";
            }
            description
              "Configure mirror destination";
          }
        }
      }
    }
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind to an interface";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Name of interface to bind to";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name to be bound under";
      }
    }
  }

  grouping dot1q-pvc-lag-grp {
    description
      "DOT1Q PVC configuration mode";
    /*
    mac-address (mac-addr)
    */
    leaf mac-address {
      if-feature mac-address;
      type yang:mac-address;
      description
        "Configure MAC address";
    }
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind to an interface";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Name of interface to bind to";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name to be bound under";
      }
    }
    /*
    description (desc-str)
    */
    leaf description {
      type string;
      description
        "set description string";
    }
    /*
    enabled
    */
    leaf enabled {
      type boolean;
      default "true";
      description
        "Shutdown the PVC";
    }
  }

  grouping cfg-dot1q-pvc-grp {
    description
      "";
    /*
    mirror { <policy (mirror-name) { in | out } [source-tag {
     dot1q (tag-id) | dot1ad (tag-id) } ] [ <ip [ ipv6 ] acl-
     counters> | ipv6 acl-counters ]> | <destination (dest-name)>
     }
    */
    container mirror {
      description
        "Configure mirror policy or destination";
      choice mirror {
        description
          "IPOS choice";
        case policy {
          list policy {
            key "policy-choice";
            max-elements 2;
            description
              "Configure mirror policy";
            leaf policy-choice {
              type enumeration {
                enum in {
                  value 0;
                  description
                    "Configure inbound mirror policy";
                }
                enum out {
                  value 1;
                  description
                    "Configure outbound mirror policy";
                }
              }
              description
                "leaf policy-choice.";
            }
            leaf mirror-name {
              type leafref {
                path "/ctxsipos:contexts/mirrorpolicyipos"
                + ":mirror-policy/mirrorpolicyipos:mirror-polname";
              }
              mandatory true;
              description
                "Mirror policy name";
            }
            container source-tag {
              description
                "Configure mirror source tag";
              choice source-tag {
                description
                  "IPOS choice";
                case dot1q {
                  leaf dot1q {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1q Ethernet type";
                  }
                }
                case dot1ad {
                  leaf dot1ad {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1ad Ethernet type";
                  }
                }
              }
            }
            choice policy-opt1 {
              description
                "IPOS choice";
              case ip {
                container ip {
                  presence "";
                  description
                    "Configure IP attributes";
                  leaf ipv6 {
                    type empty;
                    description
                      "Configure IPV6 attributes";
                  }
                  leaf acl-counters {
                    type empty;
                    mandatory true;
                    description
                      "Enable ACL counters";
                  }
                }
              }
              case ipv6-acl-counters {
                leaf ipv6-acl-counters {
                  type empty;
                  description
                    "Enable ACL counters";
                }
              }
            }
          }
        }
        case destination {
          leaf destination {
            type string {
              length "1..39";
            }
            description
              "Configure mirror destination";
          }
        }
      }
    }
    /*
    mac-address (mac-addr)
    */
    leaf mac-address {
      if-feature mac-address;
      type yang:mac-address;
      description
        "Configure MAC address";
    }
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind to an interface";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Name of interface to bind to";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name to be bound under";
      }
    }
    /*
    ipv4 multicast maximum-bandwidth (bw-val) { percent | Kbps |
     Mbps | Gbps }
    */
    container ipv4-multicast-maximum-bandwidth {
      must "not(contains(substring-after(../../../if:name, '.'), "
      + "'.') or (contains(../../../if:name, '.') and (../../pvc"
      + "/encapsulation-1qtunnel)))" {
        error-message " The multicast bandwidth option is not "
        + "allowed under QinQ PVCs.";
        description
          " The multicast bandwidth option is not allowed under
           QinQ PVCs.";
      }
      presence "";
      description
        "Set max-bandwidth for subscribers";
      leaf bw-val {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "max-bandwidth in Kbps/Mbps/Gbps/percent";
      }
      choice ipv4-multicast-maximum-bandwidth-choice {
        default Mbps;
        description
          "IPOS choice";
        case percent {
          leaf percent {
            type empty;
            description
              "Input in percentage form";
          }
        }
        case Kbps {
          leaf Kbps {
            type empty;
            description
              "Kilo bits per sec";
          }
        }
        case Mbps {
          leaf Mbps {
            type empty;
            description
              "Mega bits per sec";
          }
        }
        case Gbps {
          leaf Gbps {
            type empty;
            description
              "Giga bits per sec";
          }
        }
      }
    }
    /*
    ipv6 multicast maximum-bandwidth (bw-val) { percent | Kbps |
     Mbps | Gbps }
    */
    container ipv6-multicast-maximum-bandwidth {
      must "not(contains(substring-after(../../../if:name, '.'), "
      + "'.') or (contains(../../../if:name, '.') and (../../pvc"
      + "/encapsulation-1qtunnel)))" {
        error-message " The multicast bandwidth option is not "
        + "allowed under QinQ PVCs.";
        description
          " The multicast bandwidth option is not allowed under
           QinQ PVCs.";
      }
      presence "";
      description
        "Set max-bandwidth for subscribers";
      leaf bw-val {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "max-bandwidth in Kbps/Mbps/Gbps/percent";
      }
      choice ipv6-multicast-maximum-bandwidth-choice {
        default Mbps;
        description
          "IPOS choice";
        case percent {
          leaf percent {
            type empty;
            description
              "Input in percentage form";
          }
        }
        case Kbps {
          leaf Kbps {
            type empty;
            description
              "Kilo bits per sec";
          }
        }
        case Mbps {
          leaf Mbps {
            type empty;
            description
              "Mega bits per sec";
          }
        }
        case Gbps {
          leaf Gbps {
            type empty;
            description
              "Giga bits per sec";
          }
        }
      }
    }
  }

  grouping bvi-dot1q-grp {
    description
      "";
  }

  augment "/if:interfaces/if:interface/lagxipos:link-group" {
    description
      "ericsson-l2vlan";
    /*
    dot1q
    */
    container dot1q {
      when "(../lagxipos:encapsulation-dot1q)" {
        description
          "";
      }
      description
        "dot1q related configuration";
      container tunnel {
        description
          "dot1q tunnel configuration";
        leaf ethertype {
          type enumeration {
            enum 8100 {
              value 0;
              description
                "8100 ether type (hexadecimal)";
            }
            enum 88a8 {
              value 1;
              description
                "88a8 ether type (hexadecimal)";
            }
            enum 9100 {
              value 2;
              description
                "9100 ether type (hexadecimal) only for Spider
                 based cards";
            }
          }
          default "8100";
          description
            "dot1q tunnel ethertype configuration";
        }
      }
      list pvc {
        key "pvc-start-vlan";
        description
          "Create a dot1q pvc";
        leaf pvc-start-vlan {
          type uint16 {
            range "1..4095";
          }
          description
            "vlan-id";
        }
        leaf pvc-choice {
          type enumeration {
            enum explicit {
              value 0;
              description
                "Specifies that the configuration for the
                 individual PVCs in the range of static PVCs is
                 not expanded in the configuration file. This
                 keyword has no effect on the functionality of
                 the PVCs, but only on whether their
                 configuration is stored a range or
                 individually.";
            }
            enum on-demand {
              value 1;
              description
                "Specifies an on-demand (listening) PVC or a
                 range of on-demand PVCs. An on-demand PVC is
                 created in memory only after traffic is detected
                 on it.";
            }
          }
          mandatory true;
          description
            "leaf pvc-choice.";
        }
        leaf end-vlan {
          type uint32;
          description
            "Specify a range of vlans";
        }
        leaf profile {
          type leafref {
            path
            "/ctxsipos:contexts/ctxipos:dot1q/ctxipos:profile";
          }
          description
            "Specify a dot1q profile";
        }
        leaf link-pinning {
          if-feature link-pinning;
          type empty;
          description
            "Link pinning dot1q pvc";
        }
        uses dot1q-pvc-lag-grp;
      }
    }
  }

  augment "/if:interfaces/if:interface" {
    description
      "ericsson-l2vlan";
    /*
    dot1q
    */
    container dot1q {
      when "../if:type = 'ianaift:l2vlan'" {
        description
          "";
      }
      description
        "dot1q related configuration";
      container pvc {
        must "(/if:interfaces/if:interface[if:name = substring-b"
        + "efore(current()/../../if:name,'.')]/bvixipos:bvi/bvixipo"
        + "s:encapsulation-dot1q) or "
        + "(/if:interfaces/if:interface[if:name = substring-before("
        + "current()/../../if:name,'.')]/ethxipos:ethernet/ethxipos"
        + ":encapsulation-dot1q) or "
        + "(/if:interfaces/if:interface[if:name = substring-"
        + "before(current()/../../if:name,'.')]/lagxipos:link-"
        + "group/lagxipos:encapsulation-dot1q)" {
          error-message "%Cannot create the dot1q pvc because "
          + "encapsulation-dot1q is not enabled. Configure "
          + "encapsulation-dot1q under the parent interface first.
                  %Cannot delete/update the interface "
          + "which is parent interface for dot1q pvc. Remove the "
          + "child dot1q pvc interface and then delete/update the "
          + "interface.";
          description
            "";
        }
        presence "";
        description
          "Create a dot1q pvc";
        leaf profile {
          type leafref {
            path
            "/ctxsipos:contexts/ctxipos:dot1q/ctxipos:profile";
          }
          description
            "Specify a dot1q profile";
        }
        leaf encapsulation-1qtunnel {
          type empty;
          description
            "dot1q tunnel (1q in 1q)";
        }
        choice pvc-opt {
          description
            "IPOS choice";
          case load-balance {
            leaf load-balance {
              type empty;
              must "not(starts-with(../../../if:name, 'bvi-'))" {
                error-message "load-balance is not applicable "
                + "under BVI ports";
                description
                  "load-balance is not applicable under BVI
                   ports";
              }
              must "../../../if:type='ianaift:l2vlan'" {
                error-message "Load-balance only can be under "
                + "l2vlan mode";
                description
                  "Load-balance can be optional under l2vlan
                   mode";
              }
              description
                "Load balance dot1q pvc";
            }
          }
          case link-pinning {
            leaf link-pinning {
              if-feature link-pinning;
              type empty;
              must "not(starts-with(../../../if:name, 'bvi-'))" {
                error-message "link-pinning is not applicable "
                + "under BVI ports";
                description
                  "link-pinning is not applicable under BVI
                   ports";
              }
              must "../../../if:type='ianaift:l2vlan'" {
                error-message "Link-pinning only can be under "
                + "link-group mode";
                description
                  "Link-pinning can be optional under link-group
                   mode";
              }
              description
                "Link pinning dot1q pvc";
            }
          }
        }
        uses cfg-dot1q-pvc-grp;
      }
      list pvc-list {
        must "not(./ipv6-multicast-maximum-bandwidth and "
        + "(contains(substring-after(../../if:name, '.'), '.') or "
        + "(contains(../../if:name, '.') and (../pvc/encapsulation-"
        + "1qtunnel))))" {
          error-message "The multicast bandwidth option is not "
          + "allowed under QinQ PVCs.";
          description
            "The multicast bandwidth option is not allowed under
             QinQ PVCs.";
        }
        must "not(./ipv4-multicast-maximum-bandwidth and "
        + "(contains(substring-after(../../if:name, '.'), '.') or "
        + "(contains(../../if:name, '.') and (../pvc/encapsulation-"
        + "1qtunnel))))" {
          error-message "The multicast bandwidth option is not "
          + "allowed under QinQ PVCs.";
          description
            "The multicast bandwidth option is not allowed under
             QinQ PVCs.";
        }
        must "not(starts-with(../../if:name, 'bvi-'))" {
          error-message "pvc-list is not applicable under BVI "
          + "ports";
          description
            "pvc-list is not applicable under BVI ports";
        }
        key "inner-start-vlan";
        description
          "Create inner PVCs in the tunnel";
        leaf inner-start-vlan {
          type uint16 {
            range "1..4095";
          }
          description
            "inner vlan-id";
        }
        leaf pvc-list-choice {
          type enumeration {
            enum explicit {
              value 0;
              description
                "Specifies that the configuration for the
                 individual PVCs in the range of static PVCs is
                 not expanded in the configuration file. This
                 keyword has no effect on the functionality of
                 the PVCs, but only on whether their
                 configuration is stored a range or
                 individually.";
            }
            enum on-demand {
              value 1;
              description
                "Specifies an on-demand (listening) PVC or a
                 range of on-demand PVCs. An on-demand PVC is
                 created in memory only after traffic is detected
                 on it.";
            }
          }
          mandatory true;
          description
            "leaf pvc-list-choice.";
        }
        leaf end-vlan {
          type uint32;
          description
            "Specify a range of vlans";
        }
        leaf profile {
          type leafref {
            path
            "/ctxsipos:contexts/ctxipos:dot1q/ctxipos:profile";
          }
          description
            "Specify a dot1q profile";
        }
        choice pvc-list-opt1 {
          description
            "IPOS choice";
          case load-balance {
            leaf load-balance {
              type empty;
              description
                "Load balance dot1q pvc";
            }
          }
          case link-pinning {
            leaf link-pinning {
              if-feature link-pinning;
              type empty;
              description
                "Link pinning dot1q pvc";
            }
          }
        }
        uses dot1q-pvc-grp;
      }
    }
  }

  augment "/if:interfaces/if:interface/ethxipos:ethernet" {
    description
      "ericsson-l2vlan";
    /*
    dot1q
    */
    container dot1q {
      when "(../ethxipos:encapsulation-dot1q)" {
        description
          "";
      }
      description
        "dot1q related configuration";
      container tunnel {
        description
          "dot1q tunnel configuration";
        leaf ethertype {
          type enumeration {
            enum 8100 {
              value 0;
              description
                "8100 ether type (hexadecimal)";
            }
            enum 88a8 {
              value 1;
              description
                "88a8 ether type (hexadecimal)";
            }
          }
          default "8100";
          description
            "dot1q tunnel ethertype configuration";
        }
      }
      list pvc {
        key "pvc-start-vlan";
        description
          "Create a dot1q pvc";
        leaf pvc-start-vlan {
          type uint16 {
            range "1..4095";
          }
          description
            "vlan-id";
        }
        leaf pvc-choice {
          type enumeration {
            enum explicit {
              value 0;
              description
                "Specifies that the configuration for the
                 individual PVCs in the range of static PVCs is
                 not expanded in the configuration file. This
                 keyword has no effect on the functionality of
                 the PVCs, but only on whether their
                 configuration is stored a range or
                 individually.";
            }
            enum on-demand {
              value 1;
              description
                "Specifies an on-demand (listening) PVC or a
                 range of on-demand PVCs. An on-demand PVC is
                 created in memory only after traffic is detected
                 on it.";
            }
          }
          mandatory true;
          description
            "leaf pvc-choice.";
        }
        leaf end-vlan {
          type uint32;
          description
            "Specify a range of vlans";
        }
        leaf profile {
          type leafref {
            path
            "/ctxsipos:contexts/ctxipos:dot1q/ctxipos:profile";
          }
          description
            "Specify a dot1q profile";
        }
        leaf load-balance {
          type empty;
          description
            "Load balance dot1q pvc";
        }
        uses dot1q-pvc-grp;
      }
    }
  }

}
