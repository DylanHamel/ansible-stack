module ericsson-contexts-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-contexts-ipos";

  prefix "ctxsipos";

  import ietf-yang-types {
    prefix "yang";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-contexts-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-07-15" {
    description
      "rate-limit-circuit-nd There is some issues when deleting
       rate-limit-circuit-nd in conf cli";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-06-02" {
    description
      "New commands for Dot1q";
    reference
      "rfc6020";
  }

  revision "2017-03-15" {
    description
      "add subcommand for service";
    reference
      "rfc6020";
  }

  revision "2016-12-08" {
    description
      "Remove presence of command system";
    reference
      "rfc6020";
  }

  revision "2016-07-08" {
    description
      "Add a feature name for l2 access list";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  container contexts {
    description
      "root model for set";
    /*
    l2
    */
    container l2 {
      description
        "Layer-2 configuration under contexts";
    }
    /*
    rate-limit circuit nd (cnt-num) {interval (interval-num)}
     {drop-interval (drop-interval-num)}
    */
    container rate-limit-circuit-nd {
      presence "";
      description
        "Configure rate-limit for nd";
      leaf cnt-num {
        type uint8 {
          range "1..255";
        }
        mandatory true;
        description
          "Max no of packets";
      }
      leaf interval {
        type uint8 {
          range "1..127";
        }
        mandatory true;
        description
          "Set interval for the max packets allowed";
      }
      leaf drop-interval {
        type uint8 {
          range "1..127";
        }
        mandatory true;
        description
          "Set drop-interval";
      }
    }
    /*
    service < {multiple-contexts | inter-context routing} >
    */
    container service {
      description
        "Service commands";
      leaf multiple-contexts {
        type boolean;
        default "false";
        description
          "Enable/Disable multiple-contexts";
      }
      leaf inter-context-routing {
        type empty;
        description
          "inter-context static routing between non-local
           contexts";
      }
      container load-balance {
        description
          "Config load-balancing hash key";
        container ip {
          description
            "Config load-balancing hash key for IP";
          choice ip {
            description
              "IPOS choice";
            case layer-3 {
              leaf layer-3 {
                type empty;
                description
                  "Hash key is from layer 3 header only";
              }
            }
            case layer-4 {
              leaf layer-4 {
                type empty;
                description
                  "Hash key is from layer 3 and layer 4 headers";
              }
            }
            case source-only {
              leaf source-only {
                type empty;
                description
                  "Hash key is from source IP Header";
              }
            }
          }
        }
        container link-group {
          description
            "Config load-balancing hash key for link-group";
          choice link-group {
            description
              "IPOS choice";
            case layer-3 {
              leaf layer-3 {
                type empty;
                description
                  "Hash key is from layer 3 header only";
              }
            }
            case layer-4 {
              leaf layer-4 {
                type empty;
                description
                  "Hash key is from layer 3 and layer 4 headers";
              }
            }
            case source-only {
              leaf source-only {
                type empty;
                description
                  "Hash key is from source IP Header";
              }
            }
          }
        }
        leaf mpls-payload {
          type empty;
          description
            "Config load-balancing hash key for mpls payload";
        }
        leaf prefer-local-egress {
          type empty;
          description
            "Prefer same card/pfe egress for ecmp/link-group";
        }
      }
    }
    /*
    system { lacp { priority (priority-val) | mac-addr (addr-
     val) } }
    */
    container system {
      description
        "Set system parameters";
      container lacp {
        description
          "Set system LACP parameters";
        leaf priority {
          type uint16 {
            range "0..65535";
          }
          description
            "Configure LACP system priority";
        }
        leaf mac-addr {
          type yang:mac-address;
          description
            "Configure LACP system mac-addr";
        }
      }
    }
  }

  container contexts-state {
    config "false";
    description
      "root container of state data under contexts";
  }

}
