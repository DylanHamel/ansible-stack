module ericsson-vpws-router6000 {

  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-vpws-router6000";

  prefix "vpwsrouter6000";

  import ietf-inet-types {
    prefix "inet";
  }

  import ericsson-context-router6000 {
    prefix "ctxr6k";
  }

  import ericsson-contexts-router6000 {
    prefix "ctxsr6k";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web: &lt;http://www.ericsson.com>";

  description
    "ericsson-vpws-router6000 Copyright (c) 2019 Ericsson AB. All
     rights reserved";

  revision 2019-08-06 {
    description
      "add loop-avoidance";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "2";
    yexte:correction "0";
  }

  revision 2019-06-18 {
    description
      "add hot-standby";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "1";
    yexte:correction "0";
  }

  revision 2018-11-27 {
    description
      "remove ipos choice description";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "0";
    yexte:correction "1";
  }

  revision 2018-06-05 {
    description
      "Change model namespace";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "0";
    yexte:correction "0";
  }

  revision 2018-03-06 {
    description
      "Current implementation of PCC LSP name under tunnel is made
       obsolete and a new pcc command is added which makes LSP name
       mandatory";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "1";
  }

  revision 2017-03-06 {
    description
      "initial revision";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  grouping pw-profile-backup-peer-grp {
    description
      "Pseudowire Peer-Profile Backup Peer configuration mode";
    container tunnel-lsp {
      description
        "Map to MPLS LSP tunnel";
      choice tunnel-lsp {
        case ldp {
          leaf ldp {
            type empty;
            description
              "Map to Ldp LSP(s)";
          }
        }
        case rsvp {
          leaf rsvp {
            type string;
            description
              "Map to RSVP LSP name";
          }
        }
        case isis {
          leaf isis {
            type empty;
            description
              "Map to ISIS LSP(s)";
          }
        }
        case pcc-lsp {
          leaf pcc-lsp {
            type string;
            description
              "Map to PCC LSP name";
          }
        }
        case pcc {
          container pcc {
            presence "";
            status obsolete;
            description
              "Map to PCC LSP(s)";
            choice pcc-opt {
              case pcc-lspname {
                leaf pcc-lspname {
                  type string;
                  description
                    "leaf pcc-lspname.";
                }
              }
              case mapping {
                leaf mapping {
                  type empty;
                  description
                    "use the default pseudowire mapping";
                }
              }
            }
          }
        }
      }
    }
  }

  grouping pw-peer-profile-grp {
    description
      "Pseudowire Peer Profile configuration mode";
    container peer {
      description
        "Peer address of the pseudowire";
      container ipv4 {
        description
          "container ipv4.";
        leaf ipv4 {
          type inet:ipv4-address;
          description
            "IPv4 Peer address of the pseudowire";
        }
        uses pw-profile-peer-grp;
      }
      leaf context {
        type leafref {
          path
            "/ctxsr6k:contexts/ctxr6k:context/ctxr6k:context-name";
        }
        description
          "context address";
      }
    }
    container backup-peer {
      description
        "Backup Peer address of the pseudowire";
      container ipv4 {
        description
          "container ipv4.";
        leaf ipv4 {
          type inet:ipv4-address;
          description
            "IPv4 Peer address of the pseudowire";
        }
        uses pw-profile-backup-peer-grp;
      }
      leaf context {
        type leafref {
          path
            "/ctxsr6k:contexts/ctxr6k:context/ctxr6k:context-name";
        }
        description
          "context address";
      }
    }
  }

  grouping pw-profile-peer-grp {
    description
      "Pseudowire Peer-Profile Peer configuration mode";
    container tunnel {
      description
        "Map to specific tunnel path";
      choice tunnel {
        case soft-gre {
          container soft-gre {
            presence "";
            description
              "Map to soft-gre tunnel";
            leaf source {
              type inet:ipv4-address;
              description
                "Source address (overrides the global soft-gre
                 source)";
            }
          }
        }
        case lsp {
          container lsp {
            description
              "Map to MPLS LSP tunnel";
            choice lsp {
              case ldp {
                leaf ldp {
                  type empty;
                  description
                    "Map to Ldp LSP(s)";
                }
              }
              case rsvp {
                leaf rsvp {
                  type string;
                  description
                    "Map to RSVP LSP name";
                }
              }
              case isis {
                leaf isis {
                  type empty;
                  description
                    "Map to ISIS LSP(s)";
                }
              }
              case pcc-lsp {
                leaf pcc-lsp {
                  type string;
                  description
                    "Map to PCC LSP name";
                }
              }
              case pcc {
                container pcc {
                  presence "";
                  status obsolete;
                  description
                    "Map to PCC LSP(s)";
                  choice pcc-opt {
                    case pcc-lspname {
                      leaf pcc-lspname {
                        type string;
                        description
                          "leaf pcc-lspname.";
                      }
                    }
                    case mapping {
                      leaf mapping {
                        type empty;
                        description
                          "use the default pseudowire mapping";
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    leaf control-word {
      type empty;
      description
        "Enable control word";
    }
    leaf log-up-down {
      type empty;
      description
        "Log PW state transitions";
    }
    leaf snmp-trap {
      type empty;
      description
        "send snmp traps on PW state transitions";
    }
    leaf ignore-mtu {
      type empty;
      description
        "Ignore mtu matching";
    }
    leaf auto-revert-delay {
      type uint8 {
        range "0..180";
      }
      description
        "Enable auto revertion";
    }
    leaf vc-type {
      type enumeration {
        enum ethernet {
          value 0;
          description
            "Ether (raw) mode type";
        }
        enum vlan {
          value 1;
          description
            "Vlan (tagged) mode type";
        }
      }
      description
        "PW VC type";
    }
    leaf mtu {
      type uint16 {
        range "1200..9198";
      }
      description
        "Mtu of the PW";
    }
    leaf redundancy-mode {
      type enumeration {
        enum master-slave {
          value 0;
          description
            "Master-Slave Signaling mode";
        }
        enum independent {
          value 1;
          description
            "Independent Signaling mode";
        }
      }
      description
        "redundancy mode [master-slave|independent]";
    }
    leaf hot-standby {
      type empty;
      description
        "enable hot standby mode on this pseudowire";
    }
  }

  grouping pw-instance-grp {
    description
      "Pseudowire Peer intance configuration mode";
    leaf pw-id {
      type uint32 {
        range "1..4294967295";
      }
      description
        "PW ID";
    }
    container peer-profile {
      description
        "PW peer profile";
      leaf profile-name {
        type leafref {
          path "/ctxsr6k:contexts/vpwsrouter6000:pseudowire/"
             + "vpwsrouter6000:peer-profile/"
             + "vpwsrouter6000:peer-profile";
        }
        description
          "Name of the peer profile";
      }
      choice peer-profile-opt {
        default "mpls";
        case mpls {
          container mpls {
            presence "";
            description
              "PW peer encap mpls (default mpls)";
            uses pw-instance-peer-profile-grp;
          }
        }
        case vxlan {
          container vxlan {
            presence "";
            description
              "Configure vxlan encapsulation";
            uses pw-instance-peer-profile-vxlan-grp;
          }
        }
      }
    }
    container mirror {
      description
        "Mirror Destination Portal";
      leaf destination {
        type string;
        description
          "Mirror Destination Portal";
      }
    }
    leaf license-priority {
      type uint8 {
        range "1..100";
      }
      description
        "Configure license priority for pseudowire instance";
    }
    container vlan-rewrite {
      presence "";
      description
        "Vlan-Rewrite options";
      uses pw-instance-vlan-rewrite-grp;
    }
  }

  grouping pw-instance-peer-profile-grp {
    description
      "Pseudowire instance Peer-Profile configuration mode";
    leaf signaling-proto {
      type enumeration {
        enum ldp {
          value 0;
          description
            "LDP protocol";
        }
        enum none {
          value 1;
          description
            "Static";
        }
      }
      description
        "PW signaling Protocol";
    }
    container label {
      description
        "Set the static PW labels";
      leaf in {
        type uint32 {
          range "22..251903";
        }
        description
          "Set label for inbound packets on the PW";
      }
      leaf out {
        type uint32 {
          range "22..251903";
        }
        description
          "Set label for outbound packets on the PW";
      }
    }
    container backup-peer {
      description
        "Backup Pseudowire";
      leaf ipv4 {
        type inet:ipv4-address;
        description
          "IPv4 Backup Peer address the pseudowire";
      }
      leaf pw-id {
        type uint32 {
          range "1..4294967295";
        }
        description
          "Backup PW ID";
      }
      leaf loop-avoidance {
        type empty;
        description
          "Avoid traffic loop when revert to primary PW if peer PW is
           in spoke mode";
      }
    }
  }

  grouping pw-instance-peer-profile-vxlan-grp {
    description
      "Pseudowire instance Peer-Profile-Vxlan configuration mode";
    leaf vni {
      type uint32 {
        range "1..16777215";
      }
      description
        "Configure vxlan network identifier (vni)";
    }
    leaf local-address {
      type inet:ipv4-address;
      description
        "Configure local address";
    }
  }

  grouping pw-instance-vlan-rewrite-grp {
    description
      "";
    container ingress {
      description
        "Rewrite option on ingress traffic";
      leaf seq {
        type uint8 {
          range "1";
        }
        mandatory true;
        description
          "Order of applying the rewrites";
      }
      leaf push {
        type empty;
        mandatory true;
        description
          "Push a new tag on the packet";
      }
      leaf outer {
        type empty;
        mandatory true;
        description
          "Operation applied on outer tag";
      }
      leaf dot1q {
        type uint16 {
          range "1..4094";
        }
        mandatory true;
        description
          "Rewrite vlan tags of dot1q packets";
      }
      leaf symmetric {
        type empty;
        mandatory true;
        description
          "Apply rewrite option on both direction";
      }
    }
  }

  augment "/ctxsr6k:contexts" {
    description
      "ericsson-vpws";
    container pseudowire {
      description
        "Configure Pseudowire";
      list peer-profile {
        key "peer-profile";
        description
          "Configure a Pseudowire Peer Profile.";
        leaf peer-profile {
          type string;
          description
            "Name of the profile";
        }
        uses pw-peer-profile-grp;
      }
      list instance {
        key "instance";
        description
          "Configure a Pseudowire Instance";
        leaf instance {
          type string {
            pattern "\d{1,10}|\d{1,10}\s*\-\s*\d{1,10}";
          }
          description
            "pseudowire instance identifier range 1..4294967295, also
             can config range with '-'.";
        }
        uses pw-instance-grp;
      }
    }
  }
}
