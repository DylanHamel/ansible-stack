#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json
import logging
import ruamel.yaml
from pathlib import Path
from abc import ABC, abstractmethod
from typing import Optional, Dict, List, Any, cast

from nornir import InitNornir
from nornir.core import Nornir
from nornir.plugins.functions.text import print_result


YAML = ruamel.yaml.YAML(typ="safe")


logger = logging.getLogger(__name__)


class NetdeployBase(ABC):

    VARS_FILENAME_EXTENSIONS = [".yml", ".yaml"]

    @abstractmethod
    def dict(self) -> Dict[str, Any]:
        pass

    def _print_json(self, data: str) -> None:
        print(json.dumps(data, sort_keys=True, indent=4))


class Netdeploy(NetdeployBase):
    nr: Optional[Nornir] = None
    num_workers: Optional[int]
    inventory: Optional[str]
    log_file: Optional[str]
    log_level: Optional[str]

    def __init__(
        self,
        num_workers: Optional[int] = 100,
        inventory: Optional[str] = 'inventory.yml',
        log_file: Optional[str] = 'netdeploy.log',
        log_level: Optional[str] = 'debug'
    ):
        self.num_workers = num_workers
        self.inventory = inventory
        self.log_file = log_file
        self.log_level = log_level
        self.__init_nornir()
        self.__load_vars()

    def __init_nornir(self):
        self.nr = InitNornir(
            core={
                "num_workers": self.num_workers
            },
            inventory={
                "plugin": "nornir.plugins.inventory.ansible.AnsibleInventory",
                "options": {
                    "hostsfile": self.inventory
                }
            },
            logging={
                "file": self.log_file,
                "level": self.log_level
            }
        )

    def __load_vars(self) -> None:
        def i_load_vars(task) -> None:
            data_vars = {}
            for root, subdirs, files in os.walk(f"inventories/data_vars/{task.host.get('swisscom_platform')}/{task.host.get('swisscom_device_type')}"):
                for file in files:
                    if Path(f"{root}/{file}").is_file():
                        with open(f"{root}/{file}") as f:
                            t_data_vars = cast(Dict[str, Any], YAML.load(f))
                            logger.debug(f'Get variables from {root}/{file}')

                            for key, values in t_data_vars.items():
                                logger.debug(f"key={key}, values={values}")
                                task.host[key] = values

        output = self.nr.run(task=i_load_vars)
        print_result(output)

    def print(self) -> None:
        self._print_json(self.dict())

    def print_hosts(self) -> None:
        self._print_json(self.hosts_to_dict())

    def hosts_to_dict(self) -> Dict[str, List]:
        r = list()
        for host in self.nr.inventory.hosts:
            r.append(host)

        return {
            'hosts': r
        }

    def dict(self) -> Dict[str, Any]:
        return {
            "num_workers": self.num_workers,
            "inventory": self.inventory,
            "log_file": self.log_file,
            "log_level": self.log_level
        }
