#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from abc import ABC
from typing import Optional, Dict, Any
from nornir.core.inventory import Host
from netdeploy.core.exceptions import NetdeployArgumentsTypeError


class BaseGenerator(ABC):
    host_vars: Host
    Optional[Dict[str, Any]]

    def __init__(
        self,
        host_vars: Host = None,
        formatted_data: Optional[Dict[str, Any]] = {}
    ):
        if host_vars is None:
            raise NetdeployArgumentsTypeError(
                "BaseGenerator - host_vars can't be None."
            )
        self.host_vars = host_vars
        self.formatted_data = formatted_data

    def dict(self) -> Dict[str, Any]:
        return {
            'host_vars': self.host_vars.name,
            'formatted_data': self.formatted_data
        }

    def dict_full(self) -> Dict[str, Any]:
        return {
            **{
                'formatted_data_keys()': list(self.formatted_data.keys())
            },
            **self.dict()
        }

    def print_txt(self) -> None:
        print(self.dict())

    def print(self) -> None:
        print(json.dumps(self.dict(), indent=4, sort_keys=True))

    def print_full(self) -> None:
        print(json.dumps(self.dict_full(), indent=4, sort_keys=True))
