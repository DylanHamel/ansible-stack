#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from nornir.core.inventory import Host
from typing import Optional, Dict, List, Any, cast
from netdeploy.generator.base import BaseGenerator


class EricssonGenerator(BaseGenerator):

    def __init__(
        self,
        host_vars: Host = None,
        formatted_data: Optional[Dict[str, Any]] = {}
    ):
        super().__init__(
            host_vars=host_vars,
            formatted_data=formatted_data
        )
        self.__format_context()
        self.__format_data()

    def __format_context(self) -> None:
        self.formatted_data['contexts_vars'] = dict()
        self.formatted_data['contexts_vars']['contexts'] = dict()
        if 's_contexts' in self.host_vars.keys():
            for context in self.host_vars.get('s_contexts'):
                self.formatted_data['contexts_vars']['contexts'][context.get('name')]=dict()
                for key, values in self.host_vars.items():
                    if f"y_{context.get('alias')}" in key:
                        self.formatted_data['contexts_vars']['contexts'][context.get('name')][key] = values

    def __format_data(self) -> None:
        for key, values in self.host_vars.items():
            if f"y_global" in key:
                if isinstance(values, dict):
                    for k, v in values.items():
                        self.formatted_data[k] = v

            elif f"y_context" in key:
                if isinstance(values, dict):
                    for k, v in values.items():
                        self.formatted_data['contexts_vars'][k] = v
