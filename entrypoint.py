#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netdeploy.core.runner import Netdeploy
from netdeploy.generator.ericsson import EricssonGenerator


def main():
    nd = Netdeploy(num_workers=100, inventory='inventories/inventory.yml')

    ericsson = EricssonGenerator(nd.nr.inventory.hosts['lab_a_router_02'])
    ericsson.print_full()


if __name__ == "__main__":
    main()
