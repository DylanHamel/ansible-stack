# TO DO

List of things that we have to do or improve !

## Data Models

Some improvements and tasks about the way of variables are stored

#### System

Find a way to define automatically the `dnprefix`

```yaml
---
system:
  hostname: "{{ inventory_hostname }}"
  description: "{{ inventory_hostname }}"
  dnprefix: SubNetwork=IPN,SubNetwork=AGGREGATION,SubNetwork=780,MeContext=ipe-aad780-r-an-51
```


#### ISIS

ISIS `net` can be extract from Loopback interface.

Write an Ansible Filter plugin !

```yaml
---
isis:
  isis: METRO
  net: 39.756f.1119.1111.0003.0004.0001.0100.7609.9158.00
```

#### Interfaces

Find a way to define automatically :
*  `name`
*  `type`
*  `description`
*  `bind_interfaces`

> These variables have to be extracted from BASKAL BIS_DB !

```yaml
- name: IF-AN-AAD-52@local
    type: ifxrouter6000:p2pOverLan
    description: ipe-aad780-r-an-52:LG-AN-AAD-52
    l3_interface:
      context: local
      ip:
        address:
          addr_primary_conf:
            addr: 10.69.43.126/31
      propagate_qos:
        from_ip:
          class_map: QCM-CORE-DSCP-IN
  
  - name: lag-LG-AN-AAD-52
    type: ianaift:ieee8023adLag
    descritpion: ipe-aad780-r-an-52:LG-AN-AAD-52
    enabled: true
    link_group:
      qos:
        pwfq:
          scheduling: physical-port
        policy:
          queuing: QPO-CORE-10G
      maximum_links: "{{ s_link_group.lag_maximum_links }}"
      lacp:
        active: true
        periodic_timeout:
          long: true
        bind_interfaces:
          intf_name: IF-AN-AAD-52@local
          intf_ctx: local

  - name: 1/16
    type: ianaift:ethernetCsmacd
    description: ipe-aad780-r-an-52:1/16:1707511:Aadorf/AAD-Aadorf/AAD IPSS 4
    enabled: true
    ethernet:
      port_type: 10ge
      mtu: "{{ s_mtu.mtu }}"
      link_dampening:
        link_dampening_opt:
          up: "{{ s_link_dampening.link_dampening.link_dampening_opt.up }}"
          down: "{{ s_link_dampening.link_dampening.link_dampening_opt.down }}"
      link_group: lag-LG-AN-AAD-52
```