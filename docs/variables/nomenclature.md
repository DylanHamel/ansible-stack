# Variables nomenclature

All variables are stored in two differents places
* Variables for all devices in a group => `group_vars/{{ group_name }}/`
* Variables for a specific device in => `host_vars/{{ inventory_hostname }}`



## Draft

![variables.png](../images/variables.png)



## Globale Ansible variables

All variable files starting with `a_` are some files that define Ansible variables.

The goal of these variables are to define Ansible configuration.

### Example

```shell
⚡ cat inventories/lab/zrh/group_vars/all/a_vars.yml 
```

```yaml
---
ansible_python_interpreter: python3
ansible_connection: netconf
ansible_port: 830
```


## YANG data models variables

All variable files starting with `y_` are some files stored based on a YANG data models.

**These variables are used to generate devices configuration files !**

### Example

```shell
⚡ cat inventories/lab/zrh/host_vars/lab_a_router_02/y_interfaces.yml 
```

```yaml
--- 
interfaces:
  - name: IF-AN-AAD-52@local
    type: ifxrouter6000:p2pOverLan
    description: ipe-aad780-r-an-52:LG-AN-AAD-52
    l3_interface:
      context: local
      ip:
        address:
          addr_primary_conf:
            addr: 10.69.43.126/31
      propagate_qos:
        from_ip:
          class_map: QCM-CORE-DSCP-IN
```


## Swisscom variables

All variable files starting with `s_` are some files that define Swisscom variables.

The goal of these variables is to store some variables that is not defined in a YANG data models and are not about Ansible configuration.

### Example

```shell
⚡ cat inventories/lab/zrh/host_vars/lab_a_router_02/s_links.yml
```

```yaml
---
links:
  - circuit_code: 1601601
    m1400: Gland-1196
    neighbor_name: ipe-lss690-r-en-51
    neighbor_port: 1/12
    link_speed: 10ge%  
```