# Issues

List of open points that are to be defined to go ahead with automation.

## LAG

Based on BASKAL information, how is it possible to define that we would like to create a LAG ???

```yaml
---
s_links:
  - circuit_code: 987987987
    m1400: A String to Define Location
    local_port: 1/16
    local_sfp: BI-10GE
    local_name: ipe-iii111-r-en-51
    neighbor_port: 1/16
    neighbor_sfp: BI-10GE
    neighbor_name: ipe-jjj222-r-en-51
```

## NEXT ????