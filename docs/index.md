# Introduction

This is the entrypoint for documentation regarding Network Automation of IPT (IP Tranport) networks.

## Installation

To install Ansible and all dependencies :
1. Clone Gitlab repository.
2. Create a Python virtual environment.
3. Install dependencies by running Poetry.

> Python Virtual Environement
> * https://docs.python-guide.org/dev/virtualenvs/
> * https://docs.python.org/3/tutorial/venv.html

```shell
git clone {{ repository_url }}

cd network-mgmt
python3 -m venv .

pip install --upgrade pip
pip install poetry

poetry install 
```

Verify Ansible installation

```shell
⚡ ansible --version
ansible 2.9.10
  config file = /Volumes/Data/gitlab/ansible-stack/ansible.cfg
  configured module search path = ['/Users/dylan.hamel/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /Volumes/Data/gitlab/ansible-stack/lib/python3.7/site-packages/ansible
  executable location = /Volumes/Data/gitlab/ansible-stack/bin/ansible
  python version = 3.7.0 (v3.7.0:1bf9cc5093, Jun 26 2018, 23:26:24) [Clang 6.0 (clang-600.0.57)]

⚡ ansible-playbook --version
ansible-playbook 2.9.10
  config file = /Volumes/Data/gitlab/ansible-stack/ansible.cfg
  configured module search path = ['/Users/dylan.hamel/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /Volumes/Data/gitlab/ansible-stack/lib/python3.7/site-packages/ansible
  executable location = /Volumes/Data/gitlab/ansible-stack/bin/ansible-playbook
  python version = 3.7.0 (v3.7.0:1bf9cc5093, Jun 26 2018, 23:26:24) [Clang 6.0 (clang-600.0.57)]
```