# Generate Host variables

A lot of variables are based on `links` informations.
* BFD
* ISIS
* LDP


The idea is to "templatize" `host_vars/{{ inventory_hostname }}/y_protocol.yml`

## Example for BFD

Informations like `lag_name`, `nbr_addr` and `local_addr` are directly extracted from `s_links.yml` informations

#### Content of `y_bfd.yml`

```yaml
---
bfd:
  micro_neighbors:
    - lag_name: LG-AN-AAD-52
      nbr_addr: 10.69.43.127
      local_addr: 10.69.43.126
      minimum:
        transmit_interval: "{{ s_bfd.bfd.minimum.transmit_interval }}"
        receive_interval: "{{ s_bfd.bfd.minimum.receive_interval }}"
      detection_multiplier: "{{ s_bfd.bfd.detection_multiplier }}"
      
    - lag_name: LG-EN-ZHB-51
      nbr_addr: 10.69.43.125
      local_addr: 10.69.43.124
      minimum:
        transmit_interval: "{{ s_bfd.bfd.minimum.transmit_interval }}"
        receive_interval: "{{ s_bfd.bfd.minimum.receive_interval }}"
      detection_multiplier: "{{ s_bfd.bfd.detection_multiplier }}"
```


#### Content of `s_links.yml`

```yaml
---
s_links:
  - circuit_code: 987987987
    m1400: A String to Define Location
    local_port: 1/16
    local_sfp: BI-10GE
    local_name: ipe-iii111-r-en-51
    neighbor_port: 1/16
    neighbor_sfp: BI-10GE
    neighbor_name: ipe-jjj222-r-en-51
```