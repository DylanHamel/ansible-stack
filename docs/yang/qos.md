# YANG - QOS data models

An informations is missing about QOS.

```shell
+--rw class-map* [map-name]
        +--rw map-name              string
        +--rw (class-map-choice)
           +--:(ip)
           |  +--rw ip!
           +--:(ethernet)
           |  +--rw ethernet!
           +--:(mpls)
           |  +--rw mpls!
           +--:(ethernet-dei)
              +--rw ethernet-dei
```

There is not information about class-map type.

This means that we have to add a function to find which `class-map-choice` is used.

To fix this problem, we decided to add `type:` key to define `class-map-choice`.

```yaml
qos:
  class_map:
    - map_name: QCM-CORE-DSCP-IN
      type: ip
      ip:
```

The same idea has been applied to define if `qos` or `xxx-maps` is used.

```shell
     +--rw class-map* [map-name]
        +--rw map-name              string
        +--rw (class-map-choice)
           +--:(ip)
           |  +--rw ip!
           |     +--rw (ip)
           |     |  +--:(in)
           |     |  |  +--rw in     empty
           |     |  +--:(out)
           |     |     +--rw out?   empty
           |     +--rw ip-map* [dscp-value]
           |     |  +--rw dscp-value    uint8
           |     |  +--rw to            empty
           |     |  +--rw qos           empty
           |     |  +--rw pd-value      uint8
           |     +--rw qos* [pd-value]
           |        +--rw pd-value      uint8
           |        +--rw to            empty
           |        +--rw qos-choice
           |           +--rw ip    uint8
```

> To define which has to be used between `ip-map*` and `qos*`.

```yaml
---
qos:
  class_map:
    - map_name: QCM-CORE-DSCP-IN
      type: ip
      ip:
        in: true
        type: maps
        ip_maps:
```