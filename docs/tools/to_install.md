# Tools

The list of tools to have to work with Automation



## Markdown

| Tool Name | Link               |
| --------- | ------------------ |
| Typora    | https://typora.io/ |


## Code 

One of the following tools

| Tool Name          | Link                                     |
| ------------------ | ---------------------------------------- |
| Visual Studio Code | https://code.visualstudio.com/           |
| PyCharm            | https://www.jetbrains.com/pycharm/       |
| Atom               | https://atom.io/                         |
| SublimeText        | https://www.sublimetext.com/             |
| Notepad++          | https://notepad-plus-plus.org/downloads/ |

> Visual Studio Code is recommanded.
>
> Only free tool are listed here.



## Yang

| Tool Name                  | Link                                                      |
| -------------------------- | --------------------------------------------------------- |
| pyang                      | https://github.com/mbj4668/pyang                          |
| YangExplorer               | https://github.com/CiscoDevNet/yang-explorer              |
| MG-Soft YANG Explorer ($$) | https://www.mg-soft.si/download.html?product=yangexplorer |

> pyang is recommanded.

## Informations

All this stack is build on Ansible and Jinja2.

Ansible is written in Python.

